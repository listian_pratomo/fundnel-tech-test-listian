<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Create Game';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-login">
<h1><?= Html::encode($this->title) ?></h1>
<?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'description')->textArea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>
</div>