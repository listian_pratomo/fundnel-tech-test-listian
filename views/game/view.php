<?php
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = $game->name;
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-login">

<?php if (Yii::$app->session->hasFlash('created')){ ?>
    	<div class="alert alert-success">
            Your comment has been submited successfully.
        </div>
<?php } ?>

<h1><?= Html::encode($this->title) ?></h1>
<p><?php echo $game->description;?></p>
<p>&nbsp;</p>
<h2>Comments</h2>
<?php

echo GridView::widget([
    'dataProvider' => $dataProvider,
	'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'comment',
        'created_by',
        'created_at',
    ],
	
]);
?>
<?php if (!Yii::$app->user->isGuest) { ?>
<?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->hiddenInput()->label(false); ?>
    <?= $form->field($model, 'game_id')->hiddenInput()->label(false); ?>

    <?= $form->field($model, 'comment')->textArea(['rows' => 6])->label('Write Comment') ?>

    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>
<?php } ?>
</div>