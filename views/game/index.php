<?php

use yii\grid\GridView;
use yii\helpers\Html;



?>
<h1>Game List</h1>


<?php if (Yii::$app->session->hasFlash('updated')){ ?>
    	<div class="alert alert-success">
            Your game has been updated successfully.
        </div>
<?php } ?>

<?php if (Yii::$app->session->hasFlash('created')){ ?>
    	<div class="alert alert-success">
            Your game has been submited successfully.
        </div>
<?php } ?>

<?php

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
	'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'name',
        'description',
        'created_at',
		/*
		[
             'label'=>'Image',
             'format'=>'raw',
             'value' => function($data){
                 $url = "http://localhost/".$data->pic;
                 return Html::img($url,['alt'=>'yii']); 
             }
		],*/
        [
			'class' => 'yii\grid\ActionColumn', 'template' => '{view}    {update}',
			
			'buttons' => [
				'update' => function ($url, $model) {
					$userId = (Yii::$app->user->isGuest)? 0 : Yii::$app->user->identity->id;
					return $model->user_id == $userId ? Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
								'title' => Yii::t('app', 'Visualizar'),                       
					]) : '';
				}]
		],
    ],
	
]);
if (!Yii::$app->user->isGuest)
{
?>
						
						<button  class="btn btn-primary" name="contact-button"  onclick="window.location.href='<?php echo  Yii::$app->urlManager->createUrl("game/create")?>'">Add New</button>                    
						
<?php 
}
?>
