<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * RegisterForm is the model behind the Register form.
 */
class CommentForm extends Model
{
    public $game_id;
    public $user_id;
    public $comment;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['game_id', 'user_id',  'comment'], 'required'],
        ];
    }
	
	
	
	public function save()
	{
		$gameComment = new GameComment();
		$gameComment->game_id = $this->game_id;
		$gameComment->user_id = $this->user_id;
		$gameComment->comment = $this->comment;
		$gameComment->created_at = date('Y-m-d h:i:s');
		$gameComment->save();
	}

   
}
