<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * UpdateGameForm is the model behind the Game Creation form.
 */
class UpdateGameForm extends Model
{
    public $id;
    public $name;
    public $description;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name', 'description','id'], 'required'],
            ['name', 'uniqueName'],
        ];
    }
	
	public function uniqueName($attribute, $params)
    {
		$row = Game::find()->where(' name=\''.$this->name.'\' AND id!='.$this->id)->one();
		if ($row!=null)
          $this->addError($attribute, 'Name already exists!');
    }
	
	public function save()
	{
		$game = Game::find()->where(['id' => $this->id])->one();
		$game->name = $this->name;
		$game->description = $this->description;
		$game->updated_at = date('Y-m-d h:i:s');
		//$game->user_id = Yii::$app->user->identity->id;
		$game->save();
	}

   
}
