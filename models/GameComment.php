<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "game_comment".
 *
 * @property integer $id
 * @property integer $game_id
 * @property integer $user_id
 * @property string $comment
 * @property string $created_at
 */
class GameComment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'game_comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['game_id', 'user_id'], 'integer'],
            [['created_at'], 'safe'],
            [['comment'], 'string', 'max' => 512],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'game_id' => 'Game ID',
            'user_id' => 'User ID',
            'comment' => 'Comment',
            'created_at' => 'Created At',
        ];
    }
}
