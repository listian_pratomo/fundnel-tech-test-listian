<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * CreateGameForm is the model behind the Game Creation form.
 */
class CreateGameForm extends Model
{
    public $name;
    public $description;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name', 'description'], 'required'],
            ['name', 'uniqueName'],
        ];
    }
	
	public function uniqueName($attribute, $params)
    {
		$row = Game::find()->where(['name' => $this->name])->one();
		if ($row!=null)
          $this->addError($attribute, 'Name already exists!');
    }
	
	public function save()
	{
		$game = new Game();
		$game->name = $this->name;
		$game->description = $this->description;
		$game->created_at = date('Y-m-d h:i:s');
		$game->user_id = Yii::$app->user->identity->id;
		$game->save();
	}

   
}
