<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * RegisterForm is the model behind the Register form.
 */
class RegisterForm extends Model
{
    public $username;
    public $password;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            ['username', 'uniqueUsername'],
        ];
    }
	
	public function uniqueUsername($attribute, $params)
    {
		$row = UserLogin::find()->where(['username' => $this->username])->one();
		if ($row!=null)
          $this->addError($attribute, 'Username already exists!');
    }
	
	public function save()
	{
		$user = new UserLogin();
		$user->username = $this->username;
		$user->password = md5($this->password);
		$user->created_at = date('Y-m-d h:i:s');
		$user->save();
	}

   
}
