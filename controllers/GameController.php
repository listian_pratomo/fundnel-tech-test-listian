<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Game;
use app\models\GameComment;
use app\models\GameSearch;
use app\models\CreateGameForm;
use app\models\CommentForm;
use app\models\UpdateGameForm;
use yii\data\CArrayDataProvider;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;


class GameController extends \yii\web\Controller
{
    public function actionIndex()
    {
		$searchModel = new GameSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->get());
		$params =array(
			'dataProvider'=>$dataProvider,
			'searchModel'=>$searchModel,
		);
		return $this->render('index', $params);
    }
    
	public function actionCreate()
	{
		$model = new CreateGameForm();
         if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$model->save();
			Yii::$app->session->setFlash('created');
            return $this->redirect(Yii::$app->urlManager->createUrl("game/index"));
        }
        return $this->render('create', [
            'model' => $model,
        ]);
	}
	
	public function actionView()
	{
		$id = Yii::$app->getRequest()->getQueryParam('id');
		$game = Game::find()->where(['id' => $id])->one();
		if ($game==null)
		{
			return $this->redirect(Yii::$app->urlManager->createUrl("game/index"));
		}
		$model = null;
		if (!Yii::$app->user->isGuest) {
			$model = new CommentForm();
			$model->game_id = $id;
			$model->user_id = Yii::$app->user->identity->id;
			 if ($model->load(Yii::$app->request->post()) && $model->validate()) {
				$model->save();
				Yii::$app->session->setFlash('created');
			}
		}
		$dataProvider = new SqlDataProvider([
			'sql' => 'SELECT gc.*, u.`username` AS created_by FROM game_comment gc 
JOIN `user` u ON gc.`user_id`=u.`id` WHERE game_id='.$id,
			'sort' => [
				'attributes' => [
					'created_at',
				],
			],
		]);
        return $this->render('view', [
            'model' => $model,
            'game' => $game,
            'dataProvider' => $dataProvider,
        ]);
		
	}
	
	public function actionUpdate()
	{
		$id = Yii::$app->getRequest()->getQueryParam('id');
		$game = Game::find()->where(['id' => $id])->one();
		if ($game==null)
		{
			return $this->redirect(Yii::$app->urlManager->createUrl("game/index"));
		}
		
		$model = new UpdateGameForm();
		$model->id = $game->id;
		$model->name = $game->name;
		$model->description = $game->description;
         if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$model->save();
			Yii::$app->session->setFlash('updated');
            return $this->redirect(Yii::$app->urlManager->createUrl("game/index"));
        }
        return $this->render('update', [
            'model' => $model,
        ]);
	}

}
