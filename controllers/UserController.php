<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\RegisterForm;

class UserController extends \yii\web\Controller
{
    public function actionRegister()
    {
		$model = new RegisterForm();
         if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$model->save();
			Yii::$app->session->setFlash('registered');
            return $this->redirect(Yii::$app->urlManager->createUrl("site/login"));
        }
        return $this->render('register', [
            'model' => $model,
        ]);
    }

}
